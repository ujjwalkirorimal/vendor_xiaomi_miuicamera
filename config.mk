LOCAL_PATH := vendor/xiaomi/miuicamera

PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Copying libs/permissions etc.
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(LOCAL_PATH)/proprietary/system/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,$(LOCAL_PATH)/proprietary/system/lib64,$(TARGET_COPY_OUT_SYSTEM)/lib64) \
    $(call find-copy-subdir-files,*,$(LOCAL_PATH)/proprietary/system/priv-app/MiuiCamera/lib,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiCamera/lib) \

# MiuiCamera Package
PRODUCT_PACKAGES += \
    MiuiCamera

# Properties
PRODUCT_PRODUCT_PROPERTIES += \
    ro.miui.notch=1 \
    ro.miui.ui.version.code=14 \
    ro.miui.ui.version.name=V140 \
    ro.miui.region=CN \
    ro.miui.build.region=cn \
    ro.boot.camera.config=_pro \
    ro.com.google.lens.oem_camera_package=com.android.camera \
    ro.product.mod_device=_global

# Sepolicy
SYSTEM_EXT_PRIVATE_SEPOLICY_DIRS += \
    vendor/xiaomi/miuicamera/sepolicy/private
